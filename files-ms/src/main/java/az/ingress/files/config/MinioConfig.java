package az.ingress.files.config;

import io.minio.MinioClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class MinioConfig {

    @Value("${minio.access-key}")
    String accessKey;

    @Value("${minio.secret-key}")
    String accessSecret;

    @Value("${minio.url}")
    String minioUrl;

    @Bean
    @SneakyThrows
    public MinioClient generateMinioClient() {
        return new MinioClient(minioUrl, accessKey, accessSecret);
    }
}
