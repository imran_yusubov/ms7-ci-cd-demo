package az.ingress.files;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class FilesMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilesMsApplication.class, args);
    }

}
