package az.ingress.files.service;

import io.minio.MinioClient;
import io.minio.messages.Bucket;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

@Slf4j
@Service
public class MinioAdapter {

    private final MinioClient minioClient;
    private final String bucketName;
    private final String baseFolder;

    public MinioAdapter(MinioClient minioClient, @Value("${minio.bucket}") String defaultBucketName,
                        @Value("${minio.folder}") String defaultBaseFolder) {
        this.minioClient = minioClient;
        this.bucketName = defaultBucketName;
        this.baseFolder = defaultBaseFolder;
    }

    @SneakyThrows
    public List<Bucket> getAllBuckets() {
        return minioClient.listBuckets();
    }

    @SneakyThrows
    public void uploadFile(String name, byte[] content) {
        File file = new File("/tmp/" + name);
        try (FileOutputStream iofs = new FileOutputStream(file)) {
            file.canWrite();
            file.canRead();
            iofs.write(content);
            FilenameUtils.getExtension(file.getAbsolutePath());
            minioClient.putObject(bucketName, baseFolder + "/" + name, file.getAbsolutePath());
        }
    }

    @SneakyThrows
    public byte[] getFile(String key) {
        try (InputStream inputStream = minioClient.getObject(bucketName, baseFolder + "/" + key)) {
            return IOUtils.toByteArray(inputStream);
        }
    }

    @SneakyThrows
    @PostConstruct
    public void init() {
        log.trace("Check if {} bucket exists", bucketName);
        if (!minioClient.bucketExists(bucketName)) {
            log.trace("Creating {} bucket, as it does not exists", baseFolder);
            minioClient.makeBucket(bucketName);
        } else {
            log.trace(" {} bucket already exists", bucketName);
        }
    }
}
