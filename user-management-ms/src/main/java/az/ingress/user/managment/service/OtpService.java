package az.ingress.user.managment.service;

import az.ingress.user.managment.domain.Otp;
import az.ingress.user.managment.dto.OtpRequestDto;
import az.ingress.user.managment.dto.OtpResponseDto;

public interface OtpService {
    OtpResponseDto verifyOtp(Long otpId, OtpRequestDto dto);

    Otp generateOtp();

    Otp getOtp(Long otpId);

}
