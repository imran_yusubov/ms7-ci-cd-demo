package az.ingress.user.managment.repository;

import az.ingress.user.managment.domain.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findByUsernameAndEnabledTrue(String pin);

    @EntityGraph(attributePaths = {"authorities", "roles"})
    Optional<User> findByUsername(String pin);

    User findById(long id);

}
