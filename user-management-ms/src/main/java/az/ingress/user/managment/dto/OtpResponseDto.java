package az.ingress.user.managment.dto;

import az.ingress.user.managment.domain.enums.MethodType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtpResponseDto {
    private String temporaryUserPassword;
    private MethodType methodType;
    private long otpId;
}
