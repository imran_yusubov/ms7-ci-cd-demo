package az.ingress.user.managment.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtpRequestDto {

    @NotBlank
    @ApiModelProperty(example = "89ABCDEF-01234567-89ABCDEF")
    private String deviceId;

    @NotBlank
    @ApiModelProperty(example = "223344")
    private String otpCode;
}
