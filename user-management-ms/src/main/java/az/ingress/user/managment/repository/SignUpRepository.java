package az.ingress.user.managment.repository;

import az.ingress.user.managment.domain.SignUp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SignUpRepository extends JpaRepository<SignUp, Long> {
}
