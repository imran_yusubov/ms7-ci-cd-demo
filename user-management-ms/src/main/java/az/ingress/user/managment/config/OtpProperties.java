package az.ingress.user.managment.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@ConfigurationProperties(prefix = "otp-properties")
@Configuration
public class OtpProperties {

    @Getter
    @Setter
    private int otpLifeTime;
}
