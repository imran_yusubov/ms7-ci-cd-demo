package az.ingress.user.managment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtAccessTokenDto {

    private String token;

}
