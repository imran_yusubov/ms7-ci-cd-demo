package az.ingress.user.managment.domain.enums;

public enum MethodType {
    SIGN_UP,
    PIN_RESET;
}
