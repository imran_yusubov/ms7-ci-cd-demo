package az.ingress.release.manager.controller;

import az.ingress.release.manager.dto.ReleaseManagerDto;
import az.ingress.release.manager.service.ReleaseManagerService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping
public class ReleaseManagerController {

    private final ReleaseManagerService releaseManagerService;


    @PutMapping("/v1/release/{tag}/add")
    public void addRelease(@PathVariable("tag") String tag, @RequestBody ReleaseManagerDto releaseManagerDto) {
        releaseManagerService.addRelease(tag, releaseManagerDto);
    }

    @PutMapping("/v1/release/{tag}/remove")
    public void removeRelease(@PathVariable("tag") String tag, @RequestBody ReleaseManagerDto releaseManagerDto) {
        releaseManagerService.removeRelease(tag, releaseManagerDto);
    }

    @GetMapping("/v1/release/{tag}")
    public List<String> getRelease(@PathVariable("tag") String tag) {
        return releaseManagerService.getRelease(tag);
    }
}
