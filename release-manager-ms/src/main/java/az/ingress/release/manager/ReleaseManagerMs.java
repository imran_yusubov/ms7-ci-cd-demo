package az.ingress.release.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReleaseManagerMs {

    public static void main(String[] args) {
        SpringApplication.run(ReleaseManagerMs.class, args);
    }
}
