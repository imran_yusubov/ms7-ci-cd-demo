package az.ingress.release.manager.service;


import az.ingress.release.manager.dto.ReleaseManagerDto;

import java.util.List;

public interface ReleaseManagerService {

    void addRelease(String tag, ReleaseManagerDto releaseManagerDto);

    void removeRelease(String tag, ReleaseManagerDto releaseManagerDto);

    List<String> getRelease(String tag);
}
