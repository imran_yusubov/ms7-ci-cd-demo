package az.ingress.release.manager.repository;

import az.ingress.release.manager.domain.ReleaseManager;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReleaseManagerRepository extends JpaRepository<ReleaseManager, Long> {
    
    ReleaseManager findByTagAndName(String tag, String name);

    List<String> findAllByTag(String tag);
}
