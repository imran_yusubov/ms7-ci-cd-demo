package az.ingress.release.manager.service.impl;


import az.ingress.release.manager.domain.ReleaseManager;
import az.ingress.release.manager.dto.ReleaseManagerDto;
import az.ingress.release.manager.repository.ReleaseManagerRepository;
import az.ingress.release.manager.service.ReleaseManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ReleaseManagerServiceImpl implements ReleaseManagerService {

    private final ReleaseManagerRepository releaseManagerRepository;

    @Override
    public void addRelease(String tag, ReleaseManagerDto releaseManagerDto) {
        ReleaseManager release = new ReleaseManager(tag, releaseManagerDto.getMsName());
        releaseManagerRepository.save(release);
    }

    @Override
    public void removeRelease(String tag, ReleaseManagerDto releaseManagementDto) {
        ReleaseManager existingRelease = releaseManagerRepository.findByTagAndName(tag, releaseManagementDto.getMsName());
        releaseManagerRepository.delete(existingRelease);

    }

    @Override
    public List<String> getRelease(String tag) {
        List<String> releaseGets = releaseManagerRepository.findAllByTag(tag);
        return releaseGets;
    }
}
