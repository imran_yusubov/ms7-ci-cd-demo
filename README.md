**API Directory**

https://documenter.getpostman.com/view/7764343/TVengUQh

**Code Review Guidelines**

**Guidelines for Merge Request/MR (Pull Request/ Merge Request)**

1. Merge Request name should contain related task number and short description;
2. Merge Request description should contain what has changed;
3. Merge Request is up to date with Master/Develop (whatever base branch you are using)
4. Each Merge Request should contain checklist of prerequisites (update README, added tests, ready to merge, infrastructure docs
   etc.);
5. Merge Request should be small as practically possible;
6. The latest Merge Request can be compiled and built in CI Build pipeline. No Exceptions.

**Guidelines for controllers**

1. Controllers should without business logic;
2. Add documentation to controllers and response objects (Swagger);
3. Controllers must be as dumb as possible, move all logic to Services
4. REST API should follow naming conventions;

**Guidelines for code (Pull Request/ Merge Request)**

1. Divide methods into private methods if there are many code lines (~100);
2. Commented codes should be removed;
3. Code should follow naming conventions based on programming languages;
4. Each commit should be a single, coherent change;
5. Don’t repeat commits, use rebase;
6. Backend / Frontend Merge Request should follow their guidelines;
7. Method names should be self-documented;
8. Mappers should be without business logic;
9. Enums should be without business logic;
10. Schedulers should be without business logic;
11. Catch block must catch exact exception not any superclass;

**Guidelines for project modules and dependencies**

1. Remove unused dependencies;
2. Unused configuration properties should be removed;
3. If a method has complex logic use comments to describe it;
4. All added or updated methods are cover by unit tests in the Merge Request (90%)
5. New dependency libraries adition/replacement is agreed with the architect

**Guidelines for data model**

1. Don’t use much more variables in one model if possible divide into models;
2. DB changes general database conventions(table name, variable name and normalization rules);
3. All tables and columns in DDL must have comment (except: id, created_at, updated_at);
4. Remove unused or empty tables from database;

**Follow best practices**

1. The code has proper logging, and critical messages are logged
2. DRY principle is followed (don't repeat yourself)
3. Code follows SOLID principles
4. KISS and YAGNI principles are followed
5. Code follow style and naming conventions (standard for programming/markup/query language) Variables, classes and
   methods should be named in a way, so that it reflects their purpose
6. There are no need for code simplification to be done, such as short if, redundant local variables which is used only
   in return statement and etc.
7. Code does not bad design (includes performance issues)
8. There are no unused references, methods or variables
9. Code does not introduce any security vulnerabilities such as CSRF, XSS, Injections, Broken Auth & Session
   managementor exploits
10. Classes with gets that have computed values per get request, are not a multithread or timing liability (as output
    might vary with time)
11. Cross cutting frameworks (logging, ORM, etc) are consistent throughout the Merge Request and product. (I.e. a single logging
    framework used)
12. Mutable properties (reference types) are implemented in a way that it's not prone to NullPointerExceptions.
13. Method ordering based on access (public, protected, default , private)
14. Test cases are effective mocks, no anonym classes
15. Don't use raw types, prefer generics to avoid compiler warnings
16. Use constructor instead of builder whenever possible
17. Use project lombok to create POJO boilerplate
18. Prefer lambdas instead of imperative java code
19. API Endpoints in URL should be nouns, not verbs. Best practise:
    /farmers /crops Not recommended:
    /getFarmers /updateFarmers /deleteFarmers /getCrops /updateCrops /deleteCrops HTTP Method names should be verb .
    HTTP has defined few sets of methods which indicates the type of action to be performed on the resources. Endpoint
    name should be plural, not singular. Best practise:
    /farmers /farmers/{farmer_id} /crops /crops/{crop_id} Not recommended:
    /farmer /farmer/{farmer_id} As most modern frameworks will natively handle /farmers and /farmers/12 under a common
    controller