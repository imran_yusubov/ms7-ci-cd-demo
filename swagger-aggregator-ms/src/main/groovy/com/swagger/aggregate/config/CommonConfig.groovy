package com.swagger.aggregate.config

import logging.LoggingTcpConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(LoggingTcpConfiguration.class)
class CommonConfig {
}
